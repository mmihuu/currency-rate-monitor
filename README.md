# Currency Rate Monitor

Simple exchange currency rate checker application. 
The British pound, American dollar, 
Swiss francs, and Euro charts are available 
for you to check. You can save a report to a pdf 
file with charts. The currency rates are taken from 
http://api.nbp.pl/ and are updated every couple of minutes.


## Installation

Project written in Java 8.No need to import JavaFX library since ints
included in the Java 8 release. It should run fine after clone request.

```bash
git clone https://mmihuu@bitbucket.org/mmihuu/currency-rate-monitor.git
```

## Usage

When you run this toy app you`ve got a GUI with 4 currency rate icons. Click 
on one of the these or all simultaneously to get line chart with their 
rate for the last 30 days.    


![alt text](src/main/resources/icons/ratechecker-screenshoot.png)






## To-Do List
-  [X] add resize functionality with proper scaling 
-  [X] refactor CurrencyConverter class with Java time API to extract dates 
and rates form NBP api
- support for keyboard shortcuts in the GUI
- improve quality of save report function with better resolution
- add new charts and option to mark particular time span of choosen currency
 





