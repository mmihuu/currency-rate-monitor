package controllers;

import currencies.CurrencyConverter;
import currencies.CurrencyTable;

import currencies.CurrencyTableA;
import dialogs.DialogsUtils;
import javafx.application.Application;
import javafx.application.Platform;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.collections.ListChangeListener;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.chart.StackedAreaChart;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.MenuItem;
import javafx.scene.control.ToggleButton;
import javafx.scene.input.MouseEvent;
import savereport.SaveReportToPdf;

import java.io.IOException;
import java.util.*;

public class Controller {


    public final String nbpUrlStringUSA = "http://api.nbp.pl/api/exchangerates/rates/a/usd/last/30/?format=json";
    public final String nbpUrlStringGBP = "http://api.nbp.pl/api/exchangerates/rates/a/gbp/last/30/?format=json";
    public final String nbpUrlStringCHF = "http://api.nbp.pl/api/exchangerates/rates/a/chf/last/30/?format=json";
    public final String nbpUrlStringEUR = "http://api.nbp.pl/api/exchangerates/rates/a/eur/last/30/?format=json";


    public String customCurrencyString = "";


    @FXML
    public StackedAreaChart stackChart;
    @FXML
    public LineChart lineChart;
    @FXML
    public CategoryAxis categoryAxisLine;
    @FXML
    public NumberAxis numberAxisLine;
    @FXML
    public MenuItem menuItemExit;
    @FXML
    public MenuItem saveToPdf;


    public ArrayList<String> indexesToRemove = new ArrayList();

    public ObservableList list;
    CurrencyTable currencyTable = new CurrencyTable();
    CurrencyTableA currencyTableA = new CurrencyTableA();
    String toRemove = "";
    @FXML
    private ToggleButton funtButton;
    @FXML
    private ToggleButton dolarButton;
    @FXML
    private ToggleButton euroButton;
    @FXML
    private ToggleButton frankButton;
    @FXML
    private ChoiceBox choiceBox;


    public Controller() {

        System.out.println("Konstruktor sie odpalil");


    }

    @FXML
    void initialize() throws IOException {
        indexRemover();
        ObservableList<String> observableList = null;


        for (int i = 0; i < currencyTableA.listOfCurrencies.size(); i++) {
            observableList = FXCollections.observableArrayList(currencyTableA.listOfCurrencies.get(i).getDisplayName());
        }
        observableList.add(0,"default");
        lineChart.setTitle("Currency Rates Line Chart");

        choiceBox.setItems(observableList);


        for (int i = 0; i < currencyTableA.listOfCurrencies.size(); i++) {
            choiceBox.getItems().add(currencyTableA.listOfCurrencies.get(i).getDisplayName());
        }
        choiceBox.getItems().add("default");


        choiceBox.setOnAction(e -> getChoice(choiceBox));


    }

    private void getChoice(ChoiceBox choiceBox) {

        if(choiceBox.getValue()!="default") {
            lineChart.setCreateSymbols(false);
            Currency currency = Currency.getInstance(currencyTableA.mapFor.get(choiceBox.getValue()));

            customCurrencyString = "http://api.nbp.pl/api/exchangerates/rates/a/" + currency.getCurrencyCode().toLowerCase() + "/last/30/?format=json";

            lineChart.getData().add(currencyTable.populateLineChart(customCurrencyString, currency.getDisplayName()));





            if (lineChart.getData().size() > 1 & toRemove.length() > 2) {

                lineChart.getData().remove(indexesToRemove.indexOf(toRemove));


            }
            toRemove = "Series[" + currency.getDisplayName() + "]";
        }else{

            dolarButton.setSelected(false);
            euroButton.setSelected(false);
            frankButton.setSelected(false);
            funtButton.setSelected(false);

            lineChart.getData().clear();
            toRemove = "";
        }
    }




    public void indexRemover() {


        lineChart.getData().addListener((ListChangeListener) c -> {

            list = c.getList();
            System.out.println(list);
            int index = 0;

            for (Object o : list) {
                String s = o.toString();
                indexesToRemove.add(index, s);
                index++;
                System.out.println(list.size());

            }


        });


    }


    public void onActionButton(ActionEvent e) {


        indexRemover();


        lineChart.setCreateSymbols(false);
        lineChart.setTitle("Currency Rates Line Chart");


        if (e.getSource() == euroButton) {

            if (euroButton.isSelected()) {

                lineChart.getData().add(currencyTable.populateLineChart(nbpUrlStringEUR, "EUR"));

            }
            if (!euroButton.isSelected()) {

                lineChart.getData().remove(indexesToRemove.indexOf("Series[EUR]"));

            }


        }
        if (e.getSource() == funtButton) {


            boolean selected = funtButton.isSelected();

            if (selected) {

                lineChart.getData().add(currencyTable.populateLineChart(nbpUrlStringGBP, "GBP"));

            }

            if (!selected) {


                lineChart.getData().remove(indexesToRemove.indexOf("Series[GBP]"));


            }

        }
        if (e.getSource() == frankButton) {


            if (frankButton.isSelected()) {

                lineChart.getData().add(currencyTable.populateLineChart(nbpUrlStringCHF, "CHF"));

            }
            if (!frankButton.isSelected()) {

                lineChart.getData().remove(indexesToRemove.indexOf("Series[CHF]"));

            }


        }
        if (e.getSource() == dolarButton) {

            if (dolarButton.isSelected()) {

                lineChart.getData().add(currencyTable.populateLineChart(nbpUrlStringUSA, "USA"));


            }
            if (!dolarButton.isSelected()) {

                lineChart.getData().remove(indexesToRemove.indexOf("Series[USA]"));

            }

        }

    }


    public void menuExit() {

        Optional<ButtonType> confirmation = DialogsUtils.confirmationDialog();

        if (confirmation.get() == ButtonType.OK) {
            Platform.exit();
            System.exit(0);
        }
    }

    public void setCaspian(ActionEvent actionEvent) {
        Application.setUserAgentStylesheet(Application.STYLESHEET_CASPIAN);
    }

    public void setModena(ActionEvent actionEvent) {
        Application.setUserAgentStylesheet(Application.STYLESHEET_MODENA);

    }

    public void about(ActionEvent actionEvent) {

        DialogsUtils.dialogAboutApplication();

    }


    public void fileChooser(ActionEvent actionEvent) throws Exception {


        lineChart.getData().clear();

        SaveReportToPdf saveRaportToPdf = new SaveReportToPdf();
        saveRaportToPdf.makeRaport(lineChart, categoryAxisLine, numberAxisLine);


        dolarButton.setSelected(false);
        funtButton.setSelected(false);
        frankButton.setSelected(false);
        euroButton.setSelected(false);


    }

    public void onMouseReleased(MouseEvent mouseEvent) {

    }

    public void onMouseDragExited(MouseEvent mouseEvent) {

    }


}
