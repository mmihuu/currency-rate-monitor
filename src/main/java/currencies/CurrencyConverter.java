package currencies;


import java.io.IOException;
import java.io.InputStream;
import java.math.BigDecimal;
import java.net.URL;
import java.net.URLConnection;
import java.util.*;




public class CurrencyConverter {



    private List<BigDecimal> listOfSellPricesCurrency = new ArrayList<>();
    private List<String> listOfDateToAString = new ArrayList<>();




    public CurrencyConverter(String nbpUrlToGetCurrencyValues) {

        try {
            currencyRateAndDateToLists(getCurrencyTable(nbpUrlToGetCurrencyValues));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public String getCurrencyTable(String str) throws IOException {


        URL url = new URL(str);

        URLConnection connection = url.openConnection();
        connection.addRequestProperty("User-Agent", "Chrome");
        InputStream inputStream = connection.getInputStream();


        Scanner scanner = new Scanner(inputStream);
        StringBuilder sb = new StringBuilder();
        while (scanner.hasNextLine()) {


            sb.append(scanner.nextLine());


        }
        scanner.close();

        return sb.toString();

    }

    public void currencyRateAndDateToLists(String URLtoNbp) throws IOException {

        List<String> currencyList = new ArrayList<>(Arrays.asList(URLtoNbp
                .split("effectiveDate")));
        currencyList.removeIf(a -> a.indexOf(":") != 1);

        for (int i = 0; i < currencyList.size(); i++) {
            listOfDateToAString.add(currencyList.get(i).substring(3, 13));
            int askIndex = currencyList.get(i).indexOf("mid") + 5;
            int endIndex = currencyList.get(i).indexOf("}",askIndex) ;
            listOfSellPricesCurrency.add(new BigDecimal(currencyList.get(i).substring(askIndex,endIndex)));
        }


    }

    public List<BigDecimal> getListOfSellPricesCurrency() {
        return listOfSellPricesCurrency;
    }

    public List<String> getListOfDateToAString() {
        return listOfDateToAString;
    }
}