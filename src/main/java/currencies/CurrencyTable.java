package currencies;

import javafx.scene.chart.XYChart;

public class CurrencyTable {

    public CurrencyTable() {

    }


    public XYChart.Series populateLineChart(String nbpUrlString, String nameOfChart) {


        XYChart.Series<Double, String> xyChart = new XYChart.Series();
        CurrencyConverter converter = new CurrencyConverter(nbpUrlString);

        for (int i = 0; i < converter.getListOfDateToAString().size(); i++) {
            xyChart.getData().add(new XYChart.Data(converter.getListOfDateToAString().get(i),
                    converter.getListOfSellPricesCurrency().get(i)));

        }


        xyChart.setName(nameOfChart);


        return xyChart;
    }


}
