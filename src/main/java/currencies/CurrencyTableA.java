package currencies;


import javafx.collections.FXCollections;



import java.io.IOException;
import java.io.InputStream;


import java.net.URL;
import java.net.URLConnection;
import java.util.*;

public class CurrencyTableA {

    public String currencyTableA = "http://api.nbp.pl/api/exchangerates/tables/a/";
    public List<String> symbolsOfCurrencies = new ArrayList<>();
    public FXCollections fxCollections;

    public List<Currency> listOfCurrencies = new ArrayList<>();
    public Map<String, String> mapFor = new TreeMap<>();

    public CurrencyTableA()  {
        try {
            getCurrenciesFromTableA(getCurrencyTable(currencyTableA));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }


    public String getCurrencyTable(String URL) throws IOException {

        URL url = new URL(URL);

        URLConnection connection = url.openConnection();
        connection.addRequestProperty("User-Agent", "Chrome");
        InputStream inputStream = connection.getInputStream();


        Scanner scanner = new Scanner(inputStream);
        StringBuilder sb = new StringBuilder();
        while (scanner.hasNextLine()) {


            sb.append(scanner.nextLine());


        }
        scanner.close();

        return sb.toString();
    }

    public List<Currency> getCurrenciesFromTableA(String str) throws IOException {


        List<String> currencyList = new ArrayList<>(Arrays.asList(getCurrencyTable(currencyTableA)
                .split("currency")));
        currencyList.removeIf(a -> a.indexOf(":") != 1);


        for (int i = 0; i < currencyList.size(); i++) {
            int code = currencyList.get(i).indexOf("code");
            int mid = currencyList.get(i).indexOf("mid");

            symbolsOfCurrencies.add(currencyList.get(i).substring(code + 7, mid - 3));

            Currency instance = Currency.getInstance(currencyList.get(i).substring(code + 7, mid - 3));
            listOfCurrencies.add(instance);
            mapFor.put(instance.getDisplayName(),instance.getCurrencyCode());




        }


        return listOfCurrencies;
    }

    public FXCollections getFxCollections() {
        return fxCollections;
    }
}





