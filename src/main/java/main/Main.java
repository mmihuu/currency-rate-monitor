package main;

import javafx.application.Application;
import javafx.fxml.FXML;
import javafx.fxml.FXMLLoader;
import javafx.scene.Scene;
import javafx.scene.control.ChoiceBox;
import javafx.scene.image.Image;
import javafx.scene.layout.BorderPane;
import javafx.stage.Stage;
import sun.font.TrueTypeFont;

import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.util.Currency;
import java.util.Locale;
import java.util.ResourceBundle;
import java.util.Set;

public class Main extends Application {


    @Override
    public void start(Stage primaryStage) throws Exception {


        FXMLLoader loader = new FXMLLoader(this.getClass().getResource("/fxml/gui.fxml"));
        ResourceBundle bundle = ResourceBundle.getBundle("bundles.messages");

        loader.setResources(bundle);
        BorderPane bp = loader.load();


        Scene scene = new Scene(bp);


        primaryStage.getIcons().add(new Image("icons/app-stock-icon.png"));
        primaryStage.setScene(scene);
        primaryStage.setTitle("Currency Rate Monitor");


        primaryStage.show();


    }


    public static void main(String[] args) throws IOException, FontFormatException {








        launch(args);

    }



}