package savereport;

import com.itextpdf.io.image.ImageData;
import com.itextpdf.io.image.ImageDataFactory;
import com.itextpdf.kernel.pdf.PdfDocument;
import com.itextpdf.kernel.pdf.PdfWriter;
import com.itextpdf.layout.Document;
import com.itextpdf.layout.property.TextAlignment;
import controllers.Controller;
import currencies.CurrencyTable;
import javafx.embed.swing.SwingFXUtils;
import javafx.scene.chart.CategoryAxis;
import javafx.scene.chart.LineChart;
import javafx.scene.chart.NumberAxis;
import javafx.scene.image.Image;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;


public class SaveReportToPdf {

    CategoryAxis categoryAxis;
    NumberAxis numberAxis;
    LineChart lineChartToPdf;


    String[] stringOfLinks = new String[4];


    Controller controller = new Controller();
    CurrencyTable currencyTable = new CurrencyTable();


    int gapBetweenSceenShoot = 30;

    public LineChart valueToChart(LineChart lineChart, CategoryAxis categoryAxis, NumberAxis numberAxis, String linkDo, String
            nameOfCurrency) {


        this.categoryAxis = categoryAxis;
        this.numberAxis = numberAxis;
        lineChartToPdf = lineChart;


        lineChartToPdf.getData().add(currencyTable.populateLineChart(linkDo, nameOfCurrency));


        return lineChartToPdf;
    }


    public void makeRaport(LineChart lineChart, CategoryAxis categoryAxis, NumberAxis numberAxis) throws FileNotFoundException {


        this.stringOfLinks[0] = controller.nbpUrlStringUSA;
        this.stringOfLinks[1] = controller.nbpUrlStringGBP;
        this.stringOfLinks[2] = controller.nbpUrlStringEUR;
        this.stringOfLinks[3] = controller.nbpUrlStringCHF;


        LineChart[] seriesOfLineChart = new LineChart[4];


        Stage stage = new Stage();


        FileChooser fileChooser = new FileChooser();

        fileChooser.setInitialDirectory(new File(System.getProperty("user.home")));


        FileChooser.ExtensionFilter extensionFilter = new FileChooser.ExtensionFilter("Pdf files", "*.pdf");
        fileChooser.getExtensionFilters().

                add(extensionFilter);

        File file = fileChooser.showSaveDialog(stage);







        PdfWriter writer = null;

        if (file != null) {


            writer = new PdfWriter(new FileOutputStream(file));

            PdfDocument pdfDoc = new PdfDocument(writer);
            Document doc = new Document(pdfDoc);


            try {

                for (int i = 0; i < 4; i++) {



                    seriesOfLineChart[i] = valueToChart(lineChart, categoryAxis, numberAxis, stringOfLinks[i], stringOfLinks[i].substring(44, 47));

                    lineChart.setTitle(stringOfLinks[i].substring(44,47).toUpperCase());
                    lineChart.setLegendVisible(false);
                    Image img = seriesOfLineChart[i].snapshot(null, null);
                    ImageData imgData = ImageDataFactory.create(SwingFXUtils.fromFXImage(img, null), null);
                    com.itextpdf.layout.element.Image pdfImg = new com.itextpdf.layout.element.Image(imgData);

                    doc.setBold().setFontSize(14).setTopMargin(gapBetweenSceenShoot);
                    gapBetweenSceenShoot = 0;
                    doc.showTextAligned("Currency rates line charts of", 290, 810, TextAlignment.CENTER);
                    gapBetweenSceenShoot += 130;

                    doc.add(pdfImg);
                    String id1 = seriesOfLineChart[i].getId();
                    System.out.println(id1);

                    seriesOfLineChart[i].getData().clear();


                    lineChart.getData().clear();


                }

            } catch (Exception exc) {
                exc.printStackTrace();
            }
            doc.close();


        }

      lineChart.setTitle("Currency Rates Line Chart");

    }
}
